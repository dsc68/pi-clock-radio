#!/usr/bin/python
"""
Eric Pavey
2014-01-26

Experimenting with basic functions.  
Authored using Adafruit WebIDE talking to Rasperry Pi

Hardware:
http://www.adafruit.com/products/1109
http://learn.adafruit.com/adafruit-16x2-character-lcd-plus-keypad-for-raspberry-pi
Source needed for imports:
https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code/tree/master/Adafruit_CharLCDPlate

Adafruit_CharLCDPlate methods:
* clear() : Clear the display
* begin(16,2) : Set the size of the lcd.  I don't think this is required.
* message(string) : Displays the message
* backlight(LED COLOR) : Sets the backlight color : OFF, ON, RED, GREEN, BLUE,
    YELLOW, TEAL, VIOLET, WHITE
* buttonPressed(BUTTON) : Returns state of a single button : Note if you print
    lcd.BUTTON, you get the left number.  If buttons() returns the number on the right.
    SELECT (0,1), RIGHT (1,2), DOWN (2,4), UP (3,8), LEFT (4,16)
* buttons() : Return back all buttons pressed at once
* display() : Turn on the display
* noDisplay() : Turn off the display
"""
import sys
from time import sleep
from Adafruit_CharLCDPlate import Adafruit_CharLCDPlate

class App(Adafruit_CharLCDPlate):

    def __init__(self):
        Adafruit_CharLCDPlate.__init__(self)
        
    def run(self):
        self.clear()
        self.backlight(self.WHITE)
        self.message("Welcome!")
        sleep(1)
        
        # Main loop:
        while True:
            # sel = 1, r = 2, d = 4, u = 8, l = 16
            # Press Select & Left to exit:
            if self.buttons() == 17:   
                self.quit()   
            elif self.buttonPressed(self.UP):
                self.up()
            elif self.buttonPressed(self.DOWN):
                self.down()
            elif self.buttonPressed(self.LEFT):
                self.left()
            elif self.buttonPressed(self.RIGHT):
                self.right()
            elif self.buttonPressed(self.SELECT):
                self.select()
                
    def up(self):
        self.clear()
        self.backlight(self.GREEN)
        self.message("^ up up up\n^ UP UP UP")
    
    def down(self):
        self.clear()
        self.backlight(self.RED)
        self.message("V down down down\nV DOWN DOWN DOWN")
    
    def left(self):
        self.clear()
        self.backlight(self.YELLOW)
        self.message("< left left left\n< LEFT LEFT LEFT")
    
    def right(self):
        self.clear()
        self.backlight(self.BLUE)
        self.message("rght rght rght > \nRGHT RGHT RGHT >")
    
    def select(self):
        self.clear()
        self.backlight(self.VIOLET)
        self.message("sel sel sel \nSEL SEL SEL")
    
    def quit(self):
        # flashy!
        for i in range(3):
            for col in (self.RED, self.YELLOW, self.GREEN, self.TEAL, self.BLUE, self.VIOLET, self.WHITE):
                self.clear()
                self.backlight(col)
                self.message("! off off off !\n! OFF OFF OFF !")
                sleep(.05)
        
        self.backlight(self.OFF)
        self.noDisplay()
        sys.exit()

if __name__ == "__main__":
    App().run()

