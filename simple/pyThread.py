"""
pyThread.py

Eric Pavey
2014-02-14
Testing threading on the Pi.  Hey, it works ;)

http://docs.python.org/2/library/threading.html
"""

import threading
from time import sleep
import random

# Create an event that can be used to block other threads.
blocker = threading.Event()

def runOften():
    """
    Run every 2 seconds.
    """
    while True:
        sleep(2)
        print "Often Runs : 2 sec"

def runLessOften():
    """
    Run every 5 seconds.  Set blocker event on\off alternatly each time its ran.
    """
    while True:
        sleep(5)
        print "Runs less often 5 sec"
        if not blocker.isSet():
            blocker.set()
            print "Event : NOT BLOCKING"
        else:
            blocker.clear()
            print "Event : BLOCKING"

def runRandomly():
    """
    Won't start running until the blocker event is set.  Then runs randomly at 
    less than second itervals.
    """
    while True:
        # Block here until the blocker event is set:
        blocker.wait()
        rnd = random.random()
        sleep(rnd)
        print "Fast and Random : %s sec"%rnd
        

def main():
    try:
        threading.Thread(target=runOften).start()
        threading.Thread(target=runLessOften).start()
        threading.Thread(target=runRandomly).start()
    except Exception, e:
        print e

if __name__ == "__main__":
    main()