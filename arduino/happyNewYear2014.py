"""
 happyNewYear2010
 Eric Pavey 2009-12-31
 http://www.seeedstudio.com/wiki/images/a/a9/Electronic_Bricks_Starter_Kit_Cookbook_v1.3.pdf
 Arduino + Brick Chassis + LCD (bus 2) + sensor (A5) +
 Green LED (D8) + Red LED (D9)
 
 Sketch will print the value from the sensor to both the 
 serial port, and the LCD.  The value is converted from 0-1023
 to 0-2010 (happy new year). If there is a positive change in the
 value, the gree led will light.  If a negative change, then the
 red led will light.  If no change, no leds lit.  
""" 

# NOTE : LED's work, but the LCD doesn't.  Not sure why :(

from nanpy import Arduino as A
from nanpy import Lcd

SENSOR = 5
LEDGREEN = 8
LEDRED = 9
THEYEAR = 2014

lcd = Lcd([10, 11, 12, 13, 14, 15, 16], [2,6])
val = 0
prevVal = 1

# SETUP
A.pinMode(LEDGREEN, A.OUTPUT);
A.pinMode(LEDRED, A.OUTPUT);

# LOOP
while True:
    val = A.analogRead(SENSOR);

    if val != prevVal:
        if val < prevVal:
            A.digitalWrite(LEDRED, A.HIGH)
            A.digitalWrite(LEDGREEN, A.LOW)
            print "Val Down v"
        elif val > prevVal:
            A.digitalWrite(LEDRED, A.LOW)
            A.digitalWrite(LEDGREEN, A.HIGH)
            print "Val Up   ^"
        prevVal = val  
    else:
        A.digitalWrite(LEDRED, A.LOW)
        A.digitalWrite(LEDGREEN, A.LOW)
        print "No change..."
  
    # Print to the serial port:
    lcdYear = A.map(val, 0, 1023, 0, THEYEAR)
    print lcdYear
    #lcd.clear() # Not supported yet...?
    lcd.printString("Happy New Year!!")
    #Print to lcd:
    #if lcdYear != THEYEAR:
    #    lcd.setCursor(6,1)   
    #    lcd.printString(lcdYear) 
    #else:
    #    lcd.setCursor(4,1)
    #    lcd.printString(lcdYear)
    #    lcd.printString("!!!!") 
  
    A.delay(100)


print "ran!"

