/**
 * pylolgraph.ino
 * Aurhor: Eric Pavey - www.akeric.com - 2010-08-01
 * Hardware: Arduino + lolshield
 * Free to use, enjoy.
 * 
 * Arduino sketch designed to work with code in Python module pylolgraph.py
 *
 * Why did I make this?  Really, just for fun \ a self education experiment with
 * receiving serial data, for which this is my first time doing anything serious.
 * 
 * Will read 128 byte chunks of serial data, expected to be
 * '0' or '1' values, that are then converted to lolshield graphics.
 * Will of course work with any program sending chunks of serial data.
 * After Arduino has read the data, will send the 'X' character to
 * the serial port, to tell the sending program to send another 126
 * chunks.
 */
 
#include <Charliplexing.h>

//--------------------------------
// User Constants:
// Baud must match that of Python module:
// 300, 1200, 2400, 4800, 9600, 14400, 19200 or 28800:  Values greater than
// this seem to fail.  Must be set the same as in sending Python module.
static int BAUD = 28800;

//--------------------------------
// Program Constants:
// Width and height of lolshield:
static byte WIDTH = 14;
static byte HEIGHT = 9;
static int LEDS = WIDTH * HEIGHT;  // 126 leds
static char CHECK = 'X';

//--------------------------------

void setup() {
  LedSign::Init();
  Serial.begin(BAUD);
  // Now that the Arduino is up and running,
  // tell the Python module to start broadcasting:
  Serial.write(CHECK);
}

//--------------------------------

void loop () {
  // Don't do anything until we have the correct amount of data:
  if(Serial.available() == LEDS){
    for(int i=0;i<LEDS;i++){
      char received = Serial.read();
      byte x = (i%WIDTH);
      byte y = (i/WIDTH);    
      // Convert from character to int:
      int val = received - '0';  
      LedSign::Set(x,y,val);  
    }
    // Now that we're done, tell the port to send another series of values:
    Serial.write(CHECK);
  }
}