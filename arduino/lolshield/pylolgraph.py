"""
pylolgraph.py
Eric Pavey - www.akeric.com - 2010-08-03
Free to use, enjoy.

For use with pylolgraph.pde Arduino sketch.

Why did I make this?  Really, just for fun \ a self education experiment with
sending serial data, for which this is my first time doing anything serious.

This module is designed to be used with Pygame:  It can be used in a Pygame
application, http://www.pygame.org, capture the Pygame graphics, and send it to t
he serial port to be read and displayed by the Arduino \ lolshield:
http://www.arduino.cc/
http://jimmieprodgers.com/kits/lolshield/
Basically turning the lolshield into a very tiny 1-bit flatscreen.

This module supplies the LolShield object, which is a subclass of the Serial
object provided by the pyserial module:
http://pypi.python.org/pypi/pyserial


Pseudo-code example Pygame application:

#-----------------
import pygame
import pylolgraph

# Setup some constants:
# Create a resolution that coresponds to the lolshield.
# See docs on the setSurface() method below for acceptable resolutions.
WIDTH, HEIGHT = (448, 288)
# Baud rate must be the same as setup in pylolgraph.pde
BAUD = 28800
# Port the Arduino is on, on my WinXP box.
# PORT = 'COM4'
# Port the Arduino is on, on my Raspberry Pi:
PORT = '/dev/ttyACM0'

# Start Pygame application:
# Make surface that will have its data sent to lolshield:
screenSurf = pygame.display.set_mode((WIDTH, HEIGHT))

# Setup lolshield object:
lolshield = pylolgraph.LolShield(port=PORT, baudrate=BAUD)
# Set the surfcae that will have its pixel data sent to the Arduino\lolshield:
lolshield.setSurface(screenSurf)

# inside main loop:
    # Draw stuff to screenSurf.  Then:
    lolshield.sendData()
#-----------------
"""

from serial import Serial
import pygame.surface
import pygame.transform

__version__ = '1.0'

class LolShield(Serial):
    """
    The LolSheild object is a subclass of the Serial object since it piggybacks
    off of that functionality.
    """
    width = 14 # Width of lolshield
    height = 9 # Height of lolshield
    leds = width*height  # # Total LEDs on lolshield: 126
    # Character lolshield will send over serial port when it is ready to receive
    # another batch of serial data from Python:
    checkChar = 'X'

    def __init__(self, port, baudrate, **kwargs):
        """
        Init the Serial superclass, and setup some default surface data.
        """
        super(LolShield, self).__init__(port=port, baudrate=baudrate, **kwargs)

        # Premade surface the size of the lolshield, that will ultimately have
        # its data sampled and sent to the lolshield:
        self.lolSurf = pygame.Surface((LolShield.width, LolShield.height))

    def setSurface(self, surface):
        """
        Set the surface that will have it's pixels sent to the lolshield.
        It needs to be the same aspect ratio as the lolshield, but can be a multiple
        of the resolution.  These widths and heights are valid based on the default
        lolshield resolution of 14x9:
        14x9, 28x18, 56x36, 112x72, 224x144, 448x288, 896x576, 1792x1142, etc...
        In addition, any resolution inbetween is valid, by adding 14 and 9 to
        the width and height:  70x45, 462x297, etc.
        """
        self.surface = surface

        surfWidth, surfHeight = self.surface.get_size()
        # Make sure surface width and height are a power of the lolshield
        # width & height:
        if surfWidth % LolShield.width:
            raise Exception(("Passed in surface width must be multiple of %s, "+
                             "currently is: %s")%(LolShield.width,surfWidth))
        if surfHeight % LolShield.height:
            raise Exception(("Passed in surface width must be multiple of %s, "+
                             "currently is: %s")%(LolShield.height,surfHeight))

        # Make sure that the surface aspect ratio is the same as the lolshield:
        widthMultiple = surfWidth / LolShield.width
        heightMultiple = surfHeight / LolShield.height
        if widthMultiple != heightMultiple:
            raise Exception(("Surface aspect ratio must match that of lolshield: "+
                             "%sx%s.  Is currently %sx%s")%(LolShield.width,
                                                            LolShield.height,
                                                            surfWidth,
                                                            surfHeight))

    def sendData(self, data=None):
        """
        Update the data to be sent to the Arduino\lolshield.
        Should be executed every loop.

        data : str | list : Optional: By default will use self.surface, but instead
            can bypass it and send in direct data if any is provided.  Furthermore,
            there is built in test data:  If the string 'test' is passed in, it
            will light up the whole lolshield.  Otherwise accepts either a string with
            126 characters, or a list with 126 single entries.  Must be 0 or 1 int
            \ string values ineither case.
        """
        # Send the data to the Arduino\lolshield if it is ready to accept it.
        # This calls to the superclass Serial.readline() method.
        #if self.readline(size=1) == LolShield.checkChar:
        if self.readline(1) == LolShield.checkChar:
            # If directly passed in data, use it:
            if data:
                if data == 'test':
                    # Set all self.data values to 1 by default, to light up the
                    # whole lolshield, for testing.
                    self.data = ''.join(['1' for i in range(126)])
                else:
                    # If data is a list, convert it to a string:
                    if isinstance(data, list):
                        data = ''.join(data)
                    if len(data) != LolShield.leds:
                        raise Exception("data argument must be exactly %s values."%LolShield.leds)
                    self.data = data
            # ...otherwise process self.surface:
            else:
                # First, shrink it to match res of lolshield:
                # You can also use pygame.transform.scale() with the same args to
                # shrink it without any color blending.
                #pygame.transform.smoothscale(self.surface, (LolShield.width,
                #                                      LolShield.height), self.lolSurf)
                pygame.transform.scale(self.surface, (LolShield.width,
                                                      LolShield.height), self.lolSurf)
                # Now extract value data to send!
                # For each pixel in self.lolSurf, find the color, extract the value
                # element: Values less than 50 will be black, greater than 50 white:
                data = []
                for i in range(LolShield.leds):
                    x = i%LolShield.width
                    y = i/LolShield.width
                    col = self.lolSurf.get_at((x,y))
                    h,s,v,a = col.hsva
                    if v < 50:
                        data.append('0')
                    else:
                        data.append('1')
                self.data = ''.join(data)

            # Finally, write data to the Arduino\lolshield:
            self.write(self.data)





