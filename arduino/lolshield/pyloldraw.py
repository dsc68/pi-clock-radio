"""
pyloldraw.py
Eric Pavey - www.akeric.com - 2010-08-10
Free to use, enjoy.

Pygame application using pylolgraph.py, for drawing on the lolshield\Arduino.

Get pylolgraph.py source code: http://www.akeric.com/blog/?page_id=1180

LMB - draw
RMB - erase
'c' - clear the screen
'esc' - quit
"""

__ver__ = '1.0'
import sys
import pylolgraph

import pygame
from pygame.locals import *
pygame.init()

#--------------
# Constants

# Match that of lolshield:
WIDTH, HEIGHT = (448, 288)
FRAMERATE = 30
# Baud must match that of Arduino\lolshield sketch:
# 300, 1200, 2400, 4800, 9600, 14400, 19200 or 28800: Values greater than
# this seem to fail.
BAUD = 28800
# Port the Arduino\lolshield is on, on my winXP box:
#PORT = 'COM4'
# Port the Arduino is on, on my Raspberry Pi:
PORT = '/dev/ttyACM0'

#-------------
# screenSurf Setup
screenSurf = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("pyloldraw")
clock = pygame.time.Clock()

lolshield = pylolgraph.LolShield(port=PORT, baudrate=BAUD)
lolshield.setSurface(screenSurf)

#------------
# Main Program
def main():
    print "Running Python version:", sys.version
    print "Running PyGame version:", pygame.ver
    print "Running pyloldraw version:", pylolgraph.__version__
    looping = True

    # Main loop
    while looping:
        # Maintain our framerate:
        clock.tick(FRAMERATE)

        #-------------------------------
        # detect for events

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                looping = False
            if event.type == KEYDOWN:
                if event.key == K_c:
                    screenSurf.fill(Color('black'))
                if event.key == K_ESCAPE:
                    looping = False

        mouseButtons = pygame.mouse.get_pressed()
        if mouseButtons[0]:
            pygame.draw.circle(screenSurf, Color('white'), pygame.mouse.get_pos(), 16)
        if mouseButtons[2]:
            pygame.draw.circle(screenSurf, Color('black'), pygame.mouse.get_pos(), 16)

        #-------------------------------
        # Update the pixels to be sent to the lolshield, and send them:
        lolshield.sendData()

        #-------------------------------
        # update our display:
        pygame.display.update()

#------------
# Execution from shell\icon:
if __name__ == "__main__":
    sys.exit(main())