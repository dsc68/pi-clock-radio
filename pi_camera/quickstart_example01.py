# Example #1 from http://picamera.readthedocs.org/en/release-1.0/quickstart.html

import time
import picamera

# This does the same as below, but the below version uses the with context manager,
# Which is a bit more elegant.

#camera = picamera.PiCamera()
#try:
#    camera.start_preview()
#    time.sleep(10)
#    camera.stop_preview()
#finally:
#    camera.close()
    
with picamera.PiCamera() as camera:
    camera.start_preview()
    time.sleep(10)
    camera.stop_preview()    